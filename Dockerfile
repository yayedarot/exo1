# demarrage avec une image alpine
FROM python:3.8-alpine

# copie du requirements.txt
COPY ./requirements.txt /app/requirements.txt

# repertoire de travails
WORKDIR /app

# installation des package et dependance du requirements.txt
RUN pip install -r requirements.txt

# copie du fichier local vers l'image
COPY . /app

# configuration automatique de l'exécution du container
ENTRYPOINT [ "python" ]

CMD ["index.py" ]